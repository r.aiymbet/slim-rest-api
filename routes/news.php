<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use ReallySimpleJWT\Token;

$app = AppFactory::create();

/* $factory = new \PsrJwt\Factory\Jwt();

$builder = $factory->builder();

$token = $builder->setSecret('pass')
    ->setPayloadClaim('uid', 12)
    ->build();

echo $token->getToken(); */

$payload = [
    'iat' => time(),
    'uid' => 1,
    'exp' => time() + 10,
    'iss' => 'localhost'
];

$secret = 'pass';

$token = Token::customPayload($payload, $secret);

$result = Token::validate($token, $secret);

$app->get('/jwt', function (Request $request, Response $response) {
    $response->getBody()->write("JSON Web Token is Valid!");

    return $response;
})->add(\PsrJwt\Factory\JwtMiddleware::html('pass', '', 'Authorisation Failed'));

$app->get('/news/all', function (Request $request, Response $response) {
    $sql = "SELECT * FROM news";

    try {
        $db = new DB();
        $conn = $db->connect();

        $stmt = $conn->query($sql);
        $news = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;

        $response->getBody()->write(json_encode($news));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(200);
    } catch (PDOException $e) {
        $error = array(
            "message" => $e->getMessage()
        );

        $response->getBody()->write(json_encode($error));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }
});

$app->get('/news/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $sql = "SELECT * FROM news WHERE id = $id";

    try {
        $db = new DB();
        $conn = $db->connect();

        $stmt = $conn->query($sql);
        $new = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        $response->getBody()->write(json_encode($new));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(200);
    } catch (PDOException $e) {
        $error = array(
            "message" => $e->getMessage()
        );

        $response->getBody()->write(json_encode($error));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }
});

$app->post('/news/add', function (Request $request, Response $response, array $args) {
    
    $title = $request->getParam('title');
    $bodytext = $request->getParam('bodytext');
    $author = $request->getParam('author');
    
    $sql = "INSERT INTO news (title, bodytext, author) VALUE (:title, :bodytext, :author)";

    try {
        $db = new DB();
        $conn = $db->connect();

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':bodytext', $bodytext);
        $stmt->bindParam(':author', $author);

        $result = $stmt->execute();
        // $friend = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        $response->getBody()->write(json_encode($result));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(200);
    } catch (PDOException $e) {
        $error = array(
            "message" => $e->getMessage()
        );

        $response->getBody()->write(json_encode($error));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }
});


$app->delete('/news/delete/{id}', function (Request $request, Response $response, array $args) {
    
    $id = $args['id'];
    
    $sql = "DELETE FROM news WHERE id = $id";

    try {
        $db = new DB();
        $conn = $db->connect();

        $stmt = $conn->prepare($sql);

        $result = $stmt->execute();
        // $new = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        $response->getBody()->write(json_encode($result));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(200);
    } catch (PDOException $e) {
        $error = array(
            "message" => $e->getMessage()
        );

        $response->getBody()->write(json_encode($error));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }
});

$app->put('/news/update/{id}', function (Request $request, Response $response, array $args) {
    
    $id = $args['id'];
    $title = $request->getParam('title');
    $bodytext = $request->getParam('bodytext');
    $author = $request->getParam('author');
    
    $sql = "UPDATE news SET title=:title, bodytext=:bodytext, author=:author WHERE id = $id";

    try {
        $db = new DB();
        $conn = $db->connect();

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':bodytext', $bodytext);
        $stmt->bindParam(':author', $author);

        $result = $stmt->execute();
        // $friend = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        $response->getBody()->write(json_encode($result));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(200);
    } catch (PDOException $e) {
        $error = array(
            "message" => $e->getMessage()
        );

        $response->getBody()->write(json_encode($error));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }
});